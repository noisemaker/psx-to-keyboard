/******************************************************************
* Author: Daniele Fiori
* Version: 1.0.0 (beta)
*
* This sketch map Play Station gamepad inputs to a keyboard keys
* to use it with games that does not support gamepad.
******************************************************************/

#include <PS2X_lib.h>  //for v1.6

/******************************************************************
* set pins connected to PS2 controller:
*   - 1e column: original
*   - 2e colmun: Stef?
* replace pin numbers by the ones you use
******************************************************************/
#define PS2_DAT        11
#define PS2_CMD        12
#define PS2_SEL        10
#define PS2_CLK        9

/******************************************************************
* select modes of PS2 controller:
*   - READ_PUSHBUTTON_AS_ANALOG = analog reading of push-butttons (Pressures Mode)
*   - rumble    = motor rumbling
* uncomment 1 of the lines for each mode selection
******************************************************************/
//#define READ_PUSHBUTTON_AS_ANALOG   true
#define READ_PUSHBUTTON_AS_ANALOG   false
//#define rumble      true
#define rumble      false


#define CONTROLLER_GUITAR_HERO 2

#define DUALSHOCK_X			0
#define DUALSHOCK_CIRCLE	1
#define DUALSHOCK_TRIANGLE	2
#define DUALSHOCK_SQUARE	3
#define DUALSHOCK_R1		4
#define DUALSHOCK_R2		5
#define DUALSHOCK_L1		6
#define DUALSHOCK_L2		7
#define DUALSHOCK_UP		8
#define DUALSHOCK_LEFT		9
#define DUALSHOCK_DOWN		10
#define DUALSHOCK_RIGHT		11
#define DUALSHOCK_R3		12
#define DUALSHOCK_L3		13
#define DUALSHOCK_SELECT	14
#define DUALSHOCK_START		15
#define DUALSHOCK_AN_LX		16
#define DUALSHOCK_AN_LY		17
#define DUALSHOCK_AN_RX		18
#define DUALSHOCK_AN_RY		19
//----------------------------------
#define DUALSHOCK_BUTTONS_COUNT	20

#define ANALOG_CENTER			128
#define ANALOG_DEADZONE_RADIUS	(180 - ANALOG_CENTER)

/*
* The 8u2 keyboard firmware expects to receive 8 bytes formatted as a Keyboard HID report.
* The format has 8 byte, and structure is as follows:
* Byte 	Contents
*--------------------------------------------------------------------------------------------
*	0 	Modifier keys:
*		Bit 0 - Left CTRL
*		Bit 1 - Left SHIFT
*		Bit 2 - Left ALT
*		Bit 3 - Left GUI
*		Bit 4 - Right CTRL
*		Bit 5 - Right SHIFT
*		Bit 6 - Right ALT
*		Bit 7 - Right GUI
*-------------------------------------------------------------------------------------------
*	1 	Not used
*-------------------------------------------------------------------------------------------
* 2-7 	HID active key usage codes. This represents up to 6 keys currently being pressed.
*-------------------------------------------------------------------------------------------
*/
#define HID_KEYBOARD_BAUDRATE	9600
#define HID_KEYBOARD_REPORT_SIZE	8 // it's the standart length
#define HID_KEYBOARD_REPORT_KEYUSAGE_OFFSET	2
#define HID_KEYBOARD_REPORT_KEYUSAGE_SIZE	HID_KEYBOARD_REPORT_SIZE - HID_KEYBOARD_REPORT_KEYUSAGE_OFFSET

#define KEY_A	4
#define KEY_C	6
#define KEY_X	27	
#define KEY_RETURN_ENTER	40 // Enter/Return key
#define KEY_ESCAPE		41 // ESC key
#define KEY_ARROW_RIGHT	79
#define KEY_ARROW_LEFT	80
#define KEY_ARROW_DOWN	81
#define KEY_ARROW_UP	82
#define KEY_LEFT_CTRL	224
#define KEY_LEFT_SHIFT	225
#define KEY_LEFT_ALT	226


#define MENU_MODE_TIME_INTERVAL_MILLIS	1000

#define DEBUG


PS2X ps2x; // create PS2 Controller Class

//right now, the library does NOT support hot pluggable controllers, meaning 
//you must always either restart your Arduino after you connect the controller, 
//or call config_gamepad(pins) again after connecting the controller.

int error = 0;
byte type = 0;
byte vibrate = 0;
byte m_abButtonsState[DUALSHOCK_BUTTONS_COUNT];
bool m_bIsInMenuMode;
unsigned long m_ulLastStartPressure;

uint8_t m_aHidKeyboardReport[8] = { 0 }; 	/* Keyboard report buffer */



/*
* Get the key code (eg. A, RIGHT_ARROW, etc.) for
* each PSx PAD INPUT (both digital and analogs) when the firmware 
* is in 'NORMAL MODE' (like user is playing)
* @return The key code as HID Keyboard Key Usage
*/
byte getKeycodeForPadInputNormalMode(byte byPadInputId, byte byPadInputValue)
{
	// Digital inputs
	if (byPadInputId == DUALSHOCK_X)			{ return KEY_LEFT_CTRL; }
	else if (byPadInputId == DUALSHOCK_CIRCLE)	{ return KEY_A + 1; }
	else if (byPadInputId == DUALSHOCK_SQUARE)	{ return KEY_LEFT_ALT; }
	else if (byPadInputId == DUALSHOCK_TRIANGLE){ return KEY_A + 3; }
	else if (byPadInputId == DUALSHOCK_R1)		{ return KEY_LEFT_SHIFT; }
	else if (byPadInputId == DUALSHOCK_R2)		{ return KEY_A + 5; }
	else if (byPadInputId == DUALSHOCK_R3)		{ return KEY_A + 6; }
	else if (byPadInputId == DUALSHOCK_L1)		{ return KEY_A + 7; }
	else if (byPadInputId == DUALSHOCK_L2)		{ return KEY_A + 8; }
	else if (byPadInputId == DUALSHOCK_L3)		{ return KEY_A + 9; }
	else if (byPadInputId == DUALSHOCK_START)	{ return KEY_ESCAPE; }
	else if (byPadInputId == DUALSHOCK_SELECT)	{ return KEY_A + 11; }
	else if (byPadInputId == DUALSHOCK_UP)		{ return KEY_ARROW_UP; }
	else if (byPadInputId == DUALSHOCK_DOWN)	{ return KEY_ARROW_DOWN; }
	else if (byPadInputId == DUALSHOCK_LEFT)	{ return KEY_ARROW_LEFT; }
	else if (byPadInputId == DUALSHOCK_RIGHT)	{ return KEY_ARROW_RIGHT; }

	// Analog inputs
	else if (byPadInputId == DUALSHOCK_AN_LX)
	{
		return (isAnalogLeft(byPadInputValue) == true) ? KEY_ARROW_LEFT : KEY_ARROW_RIGHT;
	}
	else if (byPadInputId == DUALSHOCK_AN_LY)
	{
		return (isAnalogUp(byPadInputValue) == true) ? KEY_ARROW_UP : KEY_ARROW_DOWN;
	}
	else if (byPadInputId == DUALSHOCK_AN_RX)
	{
		return (isAnalogLeft(byPadInputValue) == true) ? KEY_ARROW_LEFT : KEY_ARROW_RIGHT;
	}
	else if (byPadInputId == DUALSHOCK_AN_RY)
	{
		return (isAnalogUp(byPadInputValue) == true) ? KEY_ARROW_UP : KEY_ARROW_DOWN;
	}
}

/*
* Get the key code (eg. A, RIGHT_ARROW, etc.) for
* each PSx PAD INPUT (both digital and analogs) when the firmware
* is in 'MENU MODE'.
* It is a special mode where pad button will act as typical keyboard keys
* used into game menu (arrows, enter, esc, etc.)
* @return The key code as HID Keyboard Key Usage
*/
byte getKeycodeForPadInputMenuMode(byte byPadInputId, byte byPadInputValue)
{
	// Digital inputs
	if (byPadInputId == DUALSHOCK_X)			{ return KEY_RETURN_ENTER; }
	else if (byPadInputId == DUALSHOCK_CIRCLE)	{ return 0; }
	else if (byPadInputId == DUALSHOCK_SQUARE)	{ return 0; }
	else if (byPadInputId == DUALSHOCK_TRIANGLE){ return KEY_ESCAPE; }
	else if (byPadInputId == DUALSHOCK_R1)		{ return 0; }
	else if (byPadInputId == DUALSHOCK_R2)		{ return 0; }
	else if (byPadInputId == DUALSHOCK_R3)		{ return 0; }
	else if (byPadInputId == DUALSHOCK_L1)		{ return 0; }
	else if (byPadInputId == DUALSHOCK_L2)		{ return 0; }
	else if (byPadInputId == DUALSHOCK_L3)		{ return 0; }
	else if (byPadInputId == DUALSHOCK_START)	{ return 0; }
	else if (byPadInputId == DUALSHOCK_SELECT)	{ return 0; }
	else if (byPadInputId == DUALSHOCK_UP)		{ return KEY_ARROW_UP; }
	else if (byPadInputId == DUALSHOCK_DOWN)	{ return KEY_ARROW_DOWN; }
	else if (byPadInputId == DUALSHOCK_LEFT)	{ return KEY_ARROW_LEFT; }
	else if (byPadInputId == DUALSHOCK_RIGHT)	{ return KEY_ARROW_RIGHT; }

	// Analog inputs
	else if (byPadInputId == DUALSHOCK_AN_LX)
	{
		return (isAnalogLeft(byPadInputValue) == true) ? KEY_ARROW_LEFT : KEY_ARROW_RIGHT;
	}
	else if (byPadInputId == DUALSHOCK_AN_LY)
	{
		return (isAnalogUp(byPadInputValue) == true) ? KEY_ARROW_UP : KEY_ARROW_DOWN;
	}
	else if (byPadInputId == DUALSHOCK_AN_RX)
	{
		return 0;
	}
	else if (byPadInputId == DUALSHOCK_AN_RY)
	{
		return 0;
	}
}

/*
 * Get the key code (eg. A, RIGHT_ARROW, etc.) for 
 * each PSx PAD INPUT (both digital and analogs)
 * @return The key code as HID Keyboard Key Usage
*/
byte getKeycodeForPadInput(byte byPadInputId, byte byPadInputValue)
{
	if (m_bIsInMenuMode == true)
	{
		return getKeycodeForPadInputMenuMode(byPadInputId, byPadInputValue);
	}
	else
	{
		return getKeycodeForPadInputNormalMode(byPadInputId, byPadInputValue);
	}
}



/*
 * Used to check if analog stick is into deadzone (a sort of hysteresis)
 * @return TRUE if the analog value is inside the dead zone 
 */
bool isAnalogInDeadZone(byte bValue)
{
	if (bValue >= (ANALOG_CENTER - ANALOG_DEADZONE_RADIUS) && bValue <= (ANALOG_CENTER + ANALOG_DEADZONE_RADIUS))
		return true;
	else
		return false;
}

/*
* Used to check if analog stick is UP
* @return TRUE if the analog value indicates the stick is up, FALSE otherwise
*/ 
bool isAnalogUp(byte bValue)
{
	if (isAnalogInDeadZone(bValue) == false)
	{
		return (bValue < ANALOG_CENTER);
	}
	return false;
}
/*
* Used to check if analog stick is DOWN
* @return TRUE if the analog value indicates the stick is down, FALSE otherwise
*/
bool isAnalogDown(byte bValue)
{
	if (isAnalogInDeadZone(bValue) == false)
	{
		return (bValue > ANALOG_CENTER);
	}
	return false;
}
/*
* Used to check if analog stick is LEFT
* @return TRUE if the analog value indicates the stick is left, FALSE otherwise
*/
bool isAnalogLeft(byte bValue)
{
	if (isAnalogInDeadZone(bValue) == false)
	{
		return (bValue < ANALOG_CENTER);
	}
	return false;
}
/*
* Used to check if analog stick is UP
* @return TRUE if the analog value indicates the stick is right, FALSE otherwise 
*/
bool isAnalogRight(byte bValue)
{
	if (isAnalogInDeadZone(bValue) == false)
	{
		return (bValue > ANALOG_CENTER);
	}
	return false;
}


bool isModifierKey(byte byKeyCode)
{
	if (byKeyCode == KEY_LEFT_ALT || byKeyCode == KEY_LEFT_CTRL || byKeyCode == KEY_LEFT_SHIFT)
	{
		return true;
	}
	else
		return false;
}

void setModifierKey(byte byKeyCode)
{
	/*
	*		Bit 0 - Left CTRL
	*		Bit 1 - Left SHIFT
	*		Bit 2 - Left ALT
	*		Bit 3 - Left GUI
	*		Bit 4 - Right CTRL
	*		Bit 5 - Right SHIFT
	*		Bit 6 - Right ALT
	*		Bit 7 - Right GUI
	*/
	byte byModifiersByte = m_aHidKeyboardReport[0];

	if (byKeyCode == KEY_LEFT_CTRL)
	{
		byModifiersByte = byModifiersByte | 0x01; // 0000 0001
	}
	else if (byKeyCode == KEY_LEFT_SHIFT)
	{
		byModifiersByte = byModifiersByte | 0x02;  // 0000 0010
	}
	else if (byKeyCode == KEY_LEFT_ALT)
	{
		byModifiersByte = byModifiersByte | 0x04;  // 0000 0100
	}
	//else if (byKeyCode == KEY_LEFT_GUI)
	//{
	//	byModifiersByte = byModifiersByte | 0x08;  // 000 1000
	//}


	//else if (byKeyCode == KEY_RIGHT_CTRL)
	//{
	//	byModifiersByte = byModifiersByte | 0x10;  // 0001 0000
	//}
	//else if (byKeyCode == KEY_RIGHT_SHIFT)
	//{
	//	byModifiersByte = byModifiersByte | 0x20;  // 0010 0000
	//}
	//else if (byKeyCode == KEY_RIGHT_ALT)
	//{
	//	byModifiersByte = byModifiersByte | 0x40;  // 0100 0000
	//}
	//else if (byKeyCode == KEY_RIGHT_GUI)
	//{
	//	byModifiersByte = byModifiersByte | 0x80;  // 1000 0000
	//}

	m_aHidKeyboardReport[0] = byModifiersByte;

	//Serial.print("Byte 0:");
	//Serial.print(m_aHidKeyboardReport[0], BIN);
	//Serial.println();
}

/*
 * Set the keycode of the HID Keyboard report
 */
void setKeyboardReportKeyCodes(byte byPosition, byte byKeyCode)
{
	/*Serial.print("Position:");
	Serial.print(byPosition, DEC);
	Serial.print("KeyCode:");
	Serial.print(byKeyCode, DEC);
	Serial.println();*/

	if (byPosition >= HID_KEYBOARD_REPORT_KEYUSAGE_OFFSET && byPosition < HID_KEYBOARD_REPORT_SIZE)
	{
		if (isModifierKey(byKeyCode) == true)
		{
			setModifierKey(byKeyCode);
		}
		else
		{
			m_aHidKeyboardReport[byPosition] = byKeyCode;    // Letter C
		}
	}
}

/*
 * Empty the HID Keyboard report (indicate NO KEY PRESSED)
 */
void emptyKeyboardReportBuffer()
{
	for (byte i = 0; i < HID_KEYBOARD_REPORT_SIZE; i++)
	{
		m_aHidKeyboardReport[i] = 0;
	}
}

/*
* Send the HID Keyboard report to the ATmega 8u2 which has the HID Keyboard firmware
*/
void sendKeyboardReport()
{
	//hidKeyboard.write(m_aHidKeyboardReport, 8);
	Serial.write(m_aHidKeyboardReport, 8);
}

/*
* Check if a digital input or analog input need to be send as keycode or not.
* To do this check it evaluate:
*	- digital input: is pressed?
*	- analog input: is outside of deadzone?
* @return TRUE if the input needs to be send, FALSE otherwise
*/
bool buttonOrAnalogNeedToBeSendAsKeyCode(byte byButtonOrAnagId, byte byValue)
{
	if (byButtonOrAnagId == DUALSHOCK_AN_LX || byButtonOrAnagId == DUALSHOCK_AN_LY
		|| byButtonOrAnagId == DUALSHOCK_AN_RX || byButtonOrAnagId == DUALSHOCK_AN_RY
		)
	{
		// If is an analog input check it is NOT in the dead zone
		return (isAnalogInDeadZone(byValue) == false);
	}
	else
	{
		// If is digital input, then check it is
		// pressed or not
		return (byValue == 255);
	}
}

/*
* Perform all initialization to use the
* HID Keyboard firmware on the ATmega 8u2
*/
void initializeHidKeyboard()
{
	//hidKeyboard.begin(HID_KEYBOARD_BAUDRATE);
	Serial.begin(HID_KEYBOARD_BAUDRATE);
	delay(2000);
	emptyKeyboardReportBuffer();
	sendKeyboardReport();
}





void setup()
{
	m_bIsInMenuMode = false;
	m_ulLastStartPressure = 0;
	digitalWrite(13, (m_bIsInMenuMode == true) ? HIGH : LOW);

	initializeHidKeyboard();

#ifdef DEBUG
	Serial.begin(57600);
#endif

	delay(300);  //added delay to give wireless ps2 module some time to startup, before configuring it

	//CHANGES for v1.6 HERE!!! **************PAY ATTENTION*************

	//setup pins and settings: GamePad(clock, command, attention, data, READ_PUSHBUTTON_AS_ANALOG?, Rumble?) check for error
	error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, READ_PUSHBUTTON_AS_ANALOG, rumble);
	
#ifdef DEBUG
	if (error == 0){
		Serial.print("Found Controller, configured successful ");
		Serial.print("READ_PUSHBUTTON_AS_ANALOG = ");
		if (READ_PUSHBUTTON_AS_ANALOG)
			Serial.println("true ");
		else
			Serial.println("false");
		Serial.print("rumble = ");
		if (rumble)
			Serial.println("true)");
		else
			Serial.println("false");
		Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
		Serial.println("holding L1 or R1 will print out the analog stick values.");
		Serial.println("Note: Go to www.billporter.info for updates and to report bugs.");
	}
	else if (error == 1)
		Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");

	else if (error == 2)
		Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");

	else if (error == 3)
		Serial.println("Controller refusing to enter Pressures mode, may not support it. ");
#endif

	//  Serial.print(ps2x.Analog(1), HEX);

	type = ps2x.readType();

#ifdef DEBUG
	switch (type) {
	case 0:
		Serial.print("Unknown Controller type found ");
		break;
	case 1:
		Serial.print("DualShock Controller found ");
		break;
	case 2:
		Serial.print("GuitarHero Controller found ");
		break;
	case 3:
		Serial.print("Wireless Sony DualShock Controller found ");
		break;
	}
#endif

	
}

void loop()
{
	//skip loop if no controller found
	if (error == 1)
		return;


	emptyKeyboardReportBuffer();

	/* You must Read Gamepad to get new values and set vibration values
	ps2x.read_gamepad(small motor on/off, larger motor strenght from 0-255)
	if you don't enable the rumble, use ps2x.read_gamepad(); with no values
	You should call this at least once a second
	*/

	//*************************************************************
	//* Guitar Hero Controller 
	//*************************************************************
	if (type == CONTROLLER_GUITAR_HERO)
	{
		
		ps2x.read_gamepad();          //read controller 

		if (ps2x.ButtonPressed(GREEN_FRET))
			Serial.println("Green Fret Pressed");
		if (ps2x.ButtonPressed(RED_FRET))
			Serial.println("Red Fret Pressed");
		if (ps2x.ButtonPressed(YELLOW_FRET))
			Serial.println("Yellow Fret Pressed");
		if (ps2x.ButtonPressed(BLUE_FRET))
			Serial.println("Blue Fret Pressed");
		if (ps2x.ButtonPressed(ORANGE_FRET))
			Serial.println("Orange Fret Pressed");

		if (ps2x.ButtonPressed(STAR_POWER))
			Serial.println("Star Power Command");

		if (ps2x.Button(UP_STRUM))          //will be TRUE as long as button is pressed
			Serial.println("Up Strum");
		if (ps2x.Button(DOWN_STRUM))
			Serial.println("DOWN Strum");

		if (ps2x.Button(PSB_START))         //will be TRUE as long as button is pressed
			Serial.println("Start is being held");
		if (ps2x.Button(PSB_SELECT))
			Serial.println("Select is being held");

		if (ps2x.Button(ORANGE_FRET)) {     // print stick value IF TRUE
			Serial.print("Wammy Bar Position:");
			Serial.println(ps2x.Analog(WHAMMY_BAR), DEC);
		}
	}
	//*************************************************************
	//* DualShock Controller
	//*************************************************************
	else
	{
		ps2x.read_gamepad(false, 0); //read controller and set large motor to spin at '0' speed

		// Check if user want to enter into 'menu mode'
		if (ps2x.Button(PSB_START) == true)
		{
#ifdef DEBUG
			Serial.println("Button START pressed, check if MENU MODE");
#endif
			m_ulLastStartPressure = millis();
			while (ps2x.Button(PSB_START) == true)
			{
				if ((millis() - m_ulLastStartPressure) > MENU_MODE_TIME_INTERVAL_MILLIS)
				{
					// Toggle menu mode (and notice to use by led)
					m_bIsInMenuMode = !m_bIsInMenuMode;
					digitalWrite(13, (m_bIsInMenuMode == true) ? HIGH : LOW);
					// then wait a little bit to prevent unwanted press (like debounce)
					delay(500);
					m_ulLastStartPressure = millis();
				}
				delay(50);
				ps2x.read_gamepad(false, 0); //read controller and set large motor to spin at '0' speed
			}			
		}


		ps2x.read_gamepad(false, 0); //read controller and set large motor to spin at '0' speed
		// Update digital buttons
		// (boolean Button(uint16_t) --> will be TRUE if button is being pressed)
		m_abButtonsState[DUALSHOCK_X] = (ps2x.Button(PSB_CROSS) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_CIRCLE] = (ps2x.Button(PSB_CIRCLE) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_SQUARE] = (ps2x.Button(PSB_SQUARE) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_TRIANGLE] = (ps2x.Button(PSB_TRIANGLE) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_R1] = (ps2x.Button(PSB_R1) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_R2] = (ps2x.Button(PSB_R2) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_R3] = (ps2x.Button(PSB_R3) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_L1] = (ps2x.Button(PSB_L1) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_L2] = (ps2x.Button(PSB_L2) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_L3] = (ps2x.Button(PSB_L3) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_START] = (ps2x.Button(PSB_START) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_SELECT] = (ps2x.Button(PSB_SELECT) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_UP] = (ps2x.Button(PSB_PAD_UP) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_DOWN] = (ps2x.Button(PSB_PAD_DOWN) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_LEFT] = (ps2x.Button(PSB_PAD_LEFT) == true) ? 255 : 0;
		m_abButtonsState[DUALSHOCK_RIGHT] = (ps2x.Button(PSB_PAD_RIGHT) == true) ? 255 : 0;

		// Update analogs
		m_abButtonsState[DUALSHOCK_AN_LX] = ps2x.Analog(PSS_LX);
		m_abButtonsState[DUALSHOCK_AN_LY] = ps2x.Analog(PSS_LY);
		m_abButtonsState[DUALSHOCK_AN_RX] = ps2x.Analog(PSS_RX);
		m_abButtonsState[DUALSHOCK_AN_RY] = ps2x.Analog(PSS_RY);


		// Now, check if there's any button pressed and,
		// in this case, send the matching key code to the ATmega 8U2 which
		// act as HID Keyboard
		byte byKeyUsageCount = 0;
		for (byte i = 0; i < DUALSHOCK_BUTTONS_COUNT; i++)
		{
			if (byKeyUsageCount == 6)
			{
				break;
			}
			else
			{
				/*Serial.print("Evaluate");
				Serial.print(i, DEC);
				Serial.print("=");
				Serial.print(m_abButtonsState[i], DEC);*/


				if (buttonOrAnalogNeedToBeSendAsKeyCode(i, m_abButtonsState[i]) == true)
				{
					setKeyboardReportKeyCodes(HID_KEYBOARD_REPORT_KEYUSAGE_OFFSET + byKeyUsageCount, getKeycodeForPadInput(i, m_abButtonsState[i]) );
					byKeyUsageCount++;

					/*Serial.print(i, DEC);
					Serial.print("=");
					Serial.print(m_abButtonsState[i], DEC);
					Serial.print("--> Need to send");
					Serial.println();*/

#ifdef DEBUG
					Serial.print("HID Report: ");
					for (byte j = 0; j < HID_KEYBOARD_REPORT_SIZE; j++)
					{
						Serial.print("[");	Serial.print(m_aHidKeyboardReport[j], DEC);	Serial.print("]");
					}
					Serial.println();
#endif
				}
				else
				{
					//Serial.println();
				}
			}
		}
	}

	// then send the HID Keyboard Report to ATmega 8u2
#ifndef DEBUG
	sendKeyboardReport();
#endif

	delay(50);

}
