******************************************************************
*  PSX to Keyboard Arduino Sketch
******************************************************************

Author: Daniele Fiori

Special thanks to:
- Bill Porter (https://github.com/madsci1016/Arduino-PS2X) for the library to read Playstation gamepad
- Michael Mitchell (http://mitchtech.net/arduino-usb-hid-keyboard/) for the info about using Arduino as HID keyboard
- darran(http://hunt.net.nz/users/darran/weblog/b3029/Arduino_UNO_Keyboard_HID_version_03.html and http://hunt.net.nz/users/darran/weblog/faf5e/Arduino_UNO_Keyboard_HID_part_2.html) for the ATmega 8u2 HID keyboard firmware and for the information about HID keyboards and key usages.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
<http://www.gnu.org/licenses/>

******************************************************************
* GENERAL DESCRIPTION 
******************************************************************
This Arduino sketch read the input from a PSx gamepad and let you map them to keyboards keys to use the controller with games do not support it.
There're several software solutions to do this, but I started to develop this sketch after bought Steam Link.
If you use SteamLink to play a game which does not support a gamepad, you need a keyboard to play with game. With this sketch you can plug-in your Playstation controller and play through it.

It use Arduino UNO ATmega8u2 configured as HID keyboard, so you can plug-and-play it to PC, SteamLink, etc. and is will be recognized as a USB Keyboard.
Each PSx pad input can be associated with a keyboard key.

******************************************************************
* REQUIRED MATERIAL
******************************************************************
To enjoy your PSx gamepad with this sketch you need:
1) An Arduino UNO
2) A PSx gamepad connector
3) Another Arduino board or AVR programmer like USBasp (when you flash ATmega 8u2 with the HID keyboard firmware it will not act as USB serial anymore until you flash it back to original firmware, so to upload sketch you need something which act as programmer)

******************************************************************
* HOW TO START
******************************************************************
Fist of all you need to carefully read these documents
- https://www.arduino.cc/en/Hacking/DFUProgramming8U2
- http://mitchtech.net/arduino-usb-hid-keyboard/
- http://www.billporter.info/2010/06/05/playstation-2-controller-arduino-library-v1-0/
- http://www.billporter.info/2011/03/27/arduino-playstation-2-controller-library-troubleshooting-guide/

As second step you have to download the Michael's Arduino-PS2X library
- https://github.com/madsci1016/Arduino-PS2X

Now, for the hardware connections:
- Playstation usually power the controller to 3.3v. To do initial tests I powered the PSx controller with VCC = 5V (but as Micheal say it could be risky). Running on 3.3v may be a better choice and it requires a voltage shifter on the 4 cable connected to arduino (see http://www.billporter.info/2011/03/27/arduino-playstation-2-controller-library-troubleshooting-guide). 
- I used a pull-up resistor between DATA line and VCC.

******************************************************************
* MENU MODE
******************************************************************
To better navigate into game/steam menu, I developed a 'MENU MODE'. You can activate it pressing START button on PSx pad for 1sec. When 'MENU MODE' is active led on pin 13 will light up.
In this mode buttons are mapped in this way:
- CROSS: enter/return key
- TRIANGLE: ESC key
- D-PAD: arrows (left,right,up, down)
- LEFT JOYSTICK: arrows (left,right,up, down)
Others button/analogs has no effect

******************************************************************
* WARNING
******************************************************************
Due you're acting with keyboard keycodes, if you made a mistake and send unexpected code (eg. holding down CTRL) your pc could freeze or be not controllable.
I facing these issues during development. If you run into this, don't panic: a simple RESET of the PC will solve, but you can loose your work, so ALWAYS SAVE before test it.
To avoid this problem, after I freeze my PC 2 times, now I try the sketch connecting it to a Raspberry Pi before plug it to my PC :D


